package com.webassignment.assignment.repos;

import com.webassignment.assignment.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UsersRepository extends JpaRepository<Users, Integer>, JpaSpecificationExecutor<Users> {
    Users findByUserIDAndPassword(String userID, String password);
    Users findByUserID(String userID);
    }