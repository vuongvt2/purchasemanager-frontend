package com.webassignment.assignment.model;

import java.util.HashMap;
import java.util.Map;

public class CartDTO {
    private Map<Integer, Product> cart;
    private String orderDay;

    public CartDTO(Map<Integer, Product> cart) {
        this.cart = cart;
    }

    public CartDTO() {
    }


    public void add(Product dto){
        if(this.cart == null){
            this.cart = new HashMap();
        }
        if(this.cart.containsKey(dto.getProductID())){
            int quantity = this.cart.get(dto.getProductID()).getQuantity()+1;
            dto.setQuantity(quantity);
        }
        cart.put(dto.getProductID(), dto);
    }


    public void delete(Integer id){
        if(this.cart == null){
            return;
        }
        if(this.cart.containsKey(id)){
            this.cart.remove(id);
        }
    }


    public String getOrderDay() {
        return orderDay;
    }

    public void setOrderDay(String orderDay) {
        this.orderDay = orderDay;
    }

    public Map<Integer, Product> getCart() {
        return cart;
    }

    public void setCart(Map<Integer, Product> cart) {
        this.cart = cart;
    }
}
