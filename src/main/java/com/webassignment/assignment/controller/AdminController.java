package com.webassignment.assignment.controller;

import com.webassignment.assignment.model.Product;
import com.webassignment.assignment.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
@Controller
public class AdminController {
    @Autowired
    private ProductService productService;
    // Administrator
    //Access admin page
//    @GetMapping("/adminPage")
//    public String adminPage(Model model){
//        List<Product> products = productService.getAllProduct();
//        model.addAttribute("PRODUCTLIST", products);
////        return "admin";
//        return"redirect:/";z
//    }

    //Add a new product to database
//    @PostMapping("/admin/addProduct")
    @PostMapping("/save")
    public String addProduct(@ModelAttribute("product")Product product){
        productService.addProduct(product);
        return "redirect:/";
    }
}
