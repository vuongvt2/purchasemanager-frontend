package com.webassignment.assignment.controller;

import com.webassignment.assignment.model.CartDTO;
import com.webassignment.assignment.model.Product;
import com.webassignment.assignment.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CustomerController {
    @Autowired
    private ProductService productService;

    @GetMapping("/showDetail/{productID}")
    public String showDetail(Model model, @PathVariable(name = "productID") Integer productID) {
        model.addAttribute("productDetail", productService.getAProduct(productID));
        return "productDetail";
    }

    @GetMapping("/addToCart/{productID}")
    public String addToCart(Model model, HttpServletRequest request, @PathVariable(name = "productID") Integer productID) {
        HttpSession session = request.getSession();
        CartDTO cartDTO = (CartDTO) session.getAttribute("cart");
        if (cartDTO == null) {
            cartDTO = new CartDTO();
        }
        Product product = productService.getAProduct(productID);
        product.setQuantity(1);
        cartDTO.add(product);
        System.out.println(product.getQuantity());
        session.setAttribute("cart", cartDTO);
        return "redirect:/";
    }

    @GetMapping("/cart")
    public String viewCart(Model model, HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            CartDTO cartDTO = (CartDTO) session.getAttribute("cart");
            List<Product> list = new ArrayList<>(cartDTO.getCart().values());
            Double amount = 0.0;
            for (int i = 0; i < list.toArray().length; i++) {
                amount = amount + list.get(i).getPrice() * list.get(i).getQuantity();
            }
            model.addAttribute("cartDAO", list);
            model.addAttribute("amount", amount);
        }
        return "cart";
    }

    //delete a product from Cart
    @GetMapping("/removeCart/{productID}")
    public String deleteFormCart(Model model, HttpServletRequest request, @PathVariable(name = "productID") Integer productID) {
        HttpSession session = request.getSession();
        CartDTO cartDTO = (CartDTO) session.getAttribute("cart");
        cartDTO.delete(productID);
        List<Product> list = new ArrayList<>(cartDTO.getCart().values());
        Double amount = 0.0;
        for (int i = 0; i < list.toArray().length; i++) {
            amount = amount + list.get(i).getPrice() * list.get(i).getQuantity();
        }
        model.addAttribute("amount", amount);
        session.setAttribute("cart", cartDTO);
        model.addAttribute("cartDAO", list);
        return "cart";
    }

    @GetMapping("/clearCart")
    public String clearCart( HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if(session!=null){
        session.removeAttribute("cart");
        }
        return "cart";
    }
}
