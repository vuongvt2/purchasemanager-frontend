package com.webassignment.assignment.controller;

import com.webassignment.assignment.model.Product;
import com.webassignment.assignment.model.Users;
import com.webassignment.assignment.service.ProductService;
import com.webassignment.assignment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class AppController {
    @Autowired
    private ProductService productService;
    @Autowired
    private UserService userService;
//    @Autowired
//    AuthenticationManager authenticationManager;

    @GetMapping("/login")
    public String loginPage(Model model){
        model.addAttribute("loginForm", new Users());
        return "loginAdmin";
    }
    @GetMapping("/checkLogin")
    public String checkLoginPage(Model model){
//        model.addAttribute("loginForm", new Users());
        Product product = new Product();//codeV
          model.addAttribute("product",product);//codeV
        return "createProduct";
    }

//    @PostMapping("/checkLogin")
//    public String checkLogin(Model model, @ModelAttribute("loginForm") Users loginForm,
//                             HttpServletRequest request){
//        String userName = loginForm.getUserName();
//        String password = loginForm.getPassword();
//
////        Authentication authentication = authenticationManager.authenticate(
////                new UsernamePasswordAuthenticationToken(
////                        loginForm.getUserName(),
////                        loginForm.getPassword()
////
////        );
//        if (userService.checkLogin(userName, password) != null){
//            model.addAttribute("loginMessage", "Login successful");
//            model.addAttribute("productList", productService.getAllProduct());
//            HttpSession session = request.getSession();
//            session.setAttribute("administrator", loginForm);
//            Product product = new Product();//codeV
//            model.addAttribute("product",product);//codeV
//            return "createProduct";
//        }
//        model.addAttribute("loginForm", new Users());
//        return "loginAdmin";
//    }

    @RequestMapping(value = {"/", "/home"} ,method = RequestMethod.GET)
    public String getAllProduct(Model model){
        List<Product> products = productService.getAllProduct();
        model.addAttribute("productList", products);
        return "showProduct";
    }
}
