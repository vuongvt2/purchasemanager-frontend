package com.webassignment.assignment.service;

import com.webassignment.assignment.model.Users;
import com.webassignment.assignment.repos.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    private UsersRepository repos;

    public Users checkLogin(String userID, String password){
        return repos.findByUserIDAndPassword(userID,password);
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Users user = repos.findByUserID(userName);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        Set<GrantedAuthority> grantedAuthorities = new HashSet();
        String roles = user.getRole();
        grantedAuthorities.add(new SimpleGrantedAuthority(roles));

        return new org.springframework.security.core.userdetails.User(
                user.getUserID(), user.getPassword(), grantedAuthorities);
    }
}