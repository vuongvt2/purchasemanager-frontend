package com.webassignment.assignment.service;

import com.webassignment.assignment.model.Product;
import com.webassignment.assignment.repos.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository repos;

    public List<Product> getAllProduct() {
        return repos.findAll();
    }

    public void addProduct(Product product) {
        product.setImg("../images/"+product.getImg());
        repos.save(product);
    }

    public void deleteProduct(Product product) {
        repos.delete(product);
    }

    public Product getAProduct(Integer productID) {
        return repos.getOne(productID);
    }

}
